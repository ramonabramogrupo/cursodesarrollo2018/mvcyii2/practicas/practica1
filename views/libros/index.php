<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Gestion de Libros';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="libros-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary'=>'<div class="alert b-1 bg-primary mb-2 p-2">Pagina {page} de {pageCount}</div>',
        'tableOptions' => [
            'class' => 'table table-striped table-bordered ancho-2'
        ],
        'rowOptions'=>[
            'class'=>'bg-info',
        ],
        'headerRowOptions'=>[
            'class'=>'bg-info',
        ],
        'columns' => [
            'id',
            'titulo',
            'paginas',
            [
              'attribute'=>'portada',
              'format'=>'raw',
              'value'=>function($dato){
                return Html::img("@web/imgs/$dato->portada",[
                    'class'=>'img-responsive'
                ]);
              },
              'contentOptions'=>[
                  'class'=>'w-2',
              ],
              
            ],
            'portada',
            [
              'attribute'=>'sinopsis',
              'format'=>'ntext',
              'contentOptions'=>[
                  'class'=>'p-2',
              ],
            ],
        ],
    ]); ?>
</div>
